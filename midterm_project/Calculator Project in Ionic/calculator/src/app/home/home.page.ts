import { Component } from '@angular/core';
import { isNumber } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  value = '0';
  oldValue = '0';

  lastOperator = '×';
  readyForNewInput = true;
  numberGroups = [
    [7, 8, 9, '×'],
    [4, 5, 6, '-'], 
    [1, 2, 3, '+'],
    [0, 'C', '÷', '=']
  ];

  onButtonPress(num){
    console.log(num); 

    if(isNumber(num)){
      console.log('is a number')
      if (this.readyForNewInput)
       this.value = '' + num;
      else 
       this.value += '' + num;
      this.readyForNewInput = false;
  }
  else if(num === 'C'){
    this.value = '0';
    this.readyForNewInput = true;
    }
    else if (num === '='){
      if (this.lastOperator == '×')
      this.value = '' + (parseInt(this.oldValue) * parseInt(this.value));

      if (this.lastOperator == '-')
      this.value = '' + (parseInt(this.oldValue) - parseInt(this.value));

      if (this.lastOperator == '+')
      this.value = '' + (parseInt(this.oldValue) + parseInt(this.value));
 
      if (this.lastOperator == '÷')
      this.value = '' + (parseInt(this.oldValue) / parseInt(this.value));
  }
    else{
      this.readyForNewInput = true;
      this.oldValue = this.value;
      this.lastOperator = num;
    }
  }
}